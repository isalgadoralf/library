<?php
/* @var $this UmedidaController */
/* @var $model Umedida */

$this->breadcrumbs=array(
	'Umedidas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Umedida', 'url'=>array('index')),
	array('label'=>'Manage Umedida', 'url'=>array('admin')),
);
?>

<h1>Create Umedida</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>