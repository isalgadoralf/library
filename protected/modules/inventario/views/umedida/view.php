<?php
/* @var $this UmedidaController */
/* @var $model Umedida */

$this->breadcrumbs=array(
	'Umedidas'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'List Umedida', 'url'=>array('index')),
	array('label'=>'Create Umedida', 'url'=>array('create')),
	array('label'=>'Update Umedida', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Delete Umedida', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Umedida', 'url'=>array('admin')),
);
?>

<h1>View Umedida #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'abreviatura',
		'descripcion',
	),
)); ?>
