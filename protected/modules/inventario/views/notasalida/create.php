<?php
/* @var $this NotasalidaController */
/* @var $model Notasalida */

$this->breadcrumbs=array(
	'Notasalidas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Notasalida', 'url'=>array('index')),
	array('label'=>'Manage Notasalida', 'url'=>array('admin')),
);
?>

<h1>Create Notasalida</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>