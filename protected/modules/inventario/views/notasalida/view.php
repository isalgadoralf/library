<?php
/* @var $this NotasalidaController */
/* @var $model Notasalida */

$this->breadcrumbs=array(
	'Notasalidas'=>array('index'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'List Notasalida', 'url'=>array('index')),
	array('label'=>'Create Notasalida', 'url'=>array('create')),
	array('label'=>'Update Notasalida', 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Delete Notasalida', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Notasalida', 'url'=>array('admin')),
);
?>

<h1>View Notasalida #<?php echo $model->codigo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'codigo',
		'fecha',
		'numero',
	),
)); ?>
