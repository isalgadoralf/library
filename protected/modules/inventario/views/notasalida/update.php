<?php
/* @var $this NotasalidaController */
/* @var $model Notasalida */

$this->breadcrumbs=array(
	'Notasalidas'=>array('index'),
	$model->codigo=>array('view','id'=>$model->codigo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Notasalida', 'url'=>array('index')),
	array('label'=>'Create Notasalida', 'url'=>array('create')),
	array('label'=>'View Notasalida', 'url'=>array('view', 'id'=>$model->codigo)),
	array('label'=>'Manage Notasalida', 'url'=>array('admin')),
);
?>

<h1>Update Notasalida <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>