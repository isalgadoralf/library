<?php
/* @var $this NotasalidaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Notasalidas',
);

$this->menu=array(
	array('label'=>'Create Notasalida', 'url'=>array('create')),
	array('label'=>'Manage Notasalida', 'url'=>array('admin')),
);
?>

<h1>Notasalidas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
