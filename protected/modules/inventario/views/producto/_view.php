<?php
/* @var $this ProductoController */
/* @var $data Producto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo), array('view', 'id'=>$data->codigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioc')); ?>:</b>
	<?php echo CHtml::encode($data->precioc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioV')); ?>:</b>
	<?php echo CHtml::encode($data->precioV); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock')); ?>:</b>
	<?php echo CHtml::encode($data->stock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockmin')); ?>:</b>
	<?php echo CHtml::encode($data->stockmin); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('stockmax')); ?>:</b>
	<?php echo CHtml::encode($data->stockmax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Color_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Color_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Marca_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Marca_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Area_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Area_codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Umedida_codigo')); ?>:</b>
	<?php echo CHtml::encode($data->Umedida_codigo); ?>
	<br />

	*/ ?>

</div>