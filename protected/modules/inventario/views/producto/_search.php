<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'precioc'); ?>
		<?php echo $form->textField($model,'precioc'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'precioV'); ?>
		<?php echo $form->textField($model,'precioV'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock'); ?>
		<?php echo $form->textField($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stockmin'); ?>
		<?php echo $form->textField($model,'stockmin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stockmax'); ?>
		<?php echo $form->textField($model,'stockmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Color_codigo'); ?>
		<?php echo $form->textField($model,'Color_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Marca_codigo'); ?>
		<?php echo $form->textField($model,'Marca_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Area_codigo'); ?>
		<?php echo $form->textField($model,'Area_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Umedida_codigo'); ?>
		<?php echo $form->textField($model,'Umedida_codigo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->