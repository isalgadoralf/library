<?php

/**
 * This is the model class for table "detallesalida".
 *
 * The followings are the available columns in table 'detallesalida':
 * @property integer $notasalida_codigo
 * @property integer $Producto_codigo
 * @property integer $cantidad
 * @property string $observacion
 */
class Detallesalida extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detallesalida';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notasalida_codigo, Producto_codigo', 'required'),
			array('notasalida_codigo, Producto_codigo, cantidad', 'numerical', 'integerOnly'=>true),
			array('observacion', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('notasalida_codigo, Producto_codigo, cantidad, observacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'notasalida_codigo' => 'Notasalida Codigo',
			'Producto_codigo' => 'Producto Codigo',
			'cantidad' => 'Cantidad',
			'observacion' => 'Observacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('notasalida_codigo',$this->notasalida_codigo);
		$criteria->compare('Producto_codigo',$this->Producto_codigo);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('observacion',$this->observacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Detallesalida the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
